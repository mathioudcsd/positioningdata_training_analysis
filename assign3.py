import re
import os
import numpy as np
import math
import matplotlib.pyplot as plt


#FUNCTIONS

# Type: Function
# Name: get_coord
#
# Description:
# "Creates specific regular expressions that based on them, 
# the filenames of the measurements,are used to create pairs
# of coordinates. Those coordinates represent the position of
# the cells on the plane."
# 
#
def get_coord(s):
    regex1 = re.compile(r'[a-zA-Z.]+')
    nochar = regex1.split(s)
    regex2 = re.compile(r'[_]+')
    plainnum = regex2.split(nochar[1])
    return [int(plainnum[0]), int(plainnum[1])]




#
# 83 cells with training data
# 10 APs, providing feedback
#
#TRAINING
tr_cells = [None]*84
for i in range(0,84):
    tr_cells[i]= a = [None]*10
for i in range(0,84):
    for j in range(0,10):
        tr_cells[i][j]=[]
#
# Same, with the percentiles
#
tr_prc = [None]*84
for i in range(0,84):
    tr_prc[i]= a = [None]*10
for i in range(0,84):
    for j in range(0,10):
        tr_prc[i][j]=[]

#
# 35 cells with running data
# 10 APs, providing feedback
#
#RUNNING
rn_cells = [None]*35
for i in range(0,35):
    rn_cells[i]= a = [None]*10
for i in range(0,35):
    for j in range(0,10):
        rn_cells[i][j]=[]

#
# Same, with the percentiles
#
rn_prc = [None]*35
for i in range(0,35):
    rn_prc[i]= a = [None]*10
for i in range(0,35):
    for j in range(0,10):
        rn_prc[i][j]=[]

#
# Reading the lines from the data collected in
# the files given. We append the training data to 
# temporary memory, named "training_List", in order
# to be able to later check the MAC address of the 
# AP that is  associated with the transaction.
#
i = -1
train_coords = []
for filename in os.listdir('./positioning_data/training'):
    training_List = []
    train_coords.append(get_coord(filename))
    with open('./positioning_data/training/'+filename) as f:
            line = f.readline()
            while line:
                p = re.compile(r'\s+')
                training_List.append(p.split(line))
                line = f.readline() 
    i+=1

    for node in training_List:
        if node[1] == '00:11:21:61:84:a0':
            tr_cells[i][0].append(int(node[2]))
        elif node[1] == '00:11:93:03:1e:30':
            tr_cells[i][1].append(int(node[2]))
        elif node[1] == '00:11:93:03:22:f0':
            tr_cells[i][2].append(int(node[2]))
        elif node[1] == '00:11:93:03:29:50':
            tr_cells[i][3].append(int(node[2]))
        elif node[1] == '00:15:62:51:f0:90':
            tr_cells[i][4].append(int(node[2]))
        elif node[1] == '00:15:62:51:f1:70':
            tr_cells[i][5].append(int(node[2]))
        elif node[1] == '00:15:62:51:f3:d0':
            tr_cells[i][6].append(int(node[2]))
        elif node[1] == '00:15:62:51:f4:90':
            tr_cells[i][7].append(int(node[2]))
        elif node[1] == '00:15:62:51:f6:00':
            tr_cells[i][8].append(int(node[2]))
        elif node[1] == '00:15:62:51:f7:30':
            tr_cells[i][9].append(int(node[2]))

#
# Reading the lines from the data collected in
# the files given. We append the running data to 
# temporary memory, named "running_List", in order
# to be able to later check the MAC address of the 
# AP that is  associated with the transaction.
#
i = -1
run_coords = []
for filename in os.listdir('./positioning_data/runtime_guiet'):
    running_List = []
    run_coords.append(get_coord(filename))
    with open('./positioning_data/runtime_guiet/'+filename) as f:
            line = f.readline()
            while line:
                p = re.compile(r'\s+')
                running_List.append(p.split(line))
                line = f.readline() 
    i+=1

    for node in running_List:
        if node[1] == '00:11:21:61:84:a0':
            rn_cells[i][0].append(int(node[2]))
        elif node[1] == '00:11:93:03:1e:30':
            rn_cells[i][1].append(int(node[2]))
        elif node[1] == '00:11:93:03:22:f0':
            rn_cells[i][2].append(int(node[2]))
        elif node[1] == '00:11:93:03:29:50':
            rn_cells[i][3].append(int(node[2]))
        elif node[1] == '00:15:62:51:f0:90':
            rn_cells[i][4].append(int(node[2]))
        elif node[1] == '00:15:62:51:f1:70':
            rn_cells[i][5].append(int(node[2]))
        elif node[1] == '00:15:62:51:f3:d0':
            rn_cells[i][6].append(int(node[2]))
        elif node[1] == '00:15:62:51:f4:90':
            rn_cells[i][7].append(int(node[2]))
        elif node[1] == '00:15:62:51:f6:00':
            rn_cells[i][8].append(int(node[2]))
        elif node[1] == '00:15:62:51:f7:30':
            rn_cells[i][9].append(int(node[2]))

#
# If there is no data found for a specific AP and
# specific cell, we automatically append 
# the value -100db 
#

# print train_coords
for i in range(0,84):
    for j in range(0,10):
        if not tr_cells[i][j]:
            tr_cells[i][j].append(-100)

for i in range(0,35):
    for j in range(0,10):
        if not rn_cells[i][j]:
            rn_cells[i][j].append(-100)


#
# Using the NumPy library to 
# compute the percentiles
#

# PERCENTILES
for i in range(0,84):
    for j in range(0,10):
        a = np.array(tr_cells[i][j])
        for k in range(1,11):
            tr_prc[i][j].append(np.percentile(a,(k)*10))
            # print("i={}, j={} k={}".format(i, j, k))
for i in range(0,35):
    for j in range(0,10):
        a = np.array(rn_cells[i][j])
        for k in range(1,11):
            rn_prc[i][j].append(np.percentile(a,(k)*10))
            # print("i={}, j={}, k={}".format(i, j, k))

#print rn_prc


################## Weights #####################


#
# Expressing the given formula to
# equivalent Python syntax, in order to 
# compute the weights.
# There's not much to be said here.
#

weights = [None]*35
for i in range(0,35):
    weights[i]= a = [None]*84


for k in range(0, 35):
    for l in range(0, 84):
        summ=0
        for i in range(0, 10):
            diff=0
            for j in range(0,10):
                # print("k={}, l={}, i={}, j={}, runningdata={}, trainingdata={}".format(k, l, i, j, rn_cells[k][i][j], tr_cells[l][i][j]))
                diff+=math.pow((rn_prc[k][i][j] - tr_prc[l][i][j]),2)
                # print("k={}, l={}, i={}, j={}, runningdata={}, trainingdata={}".format(k, l, i, j, rn_cells[k][i][j], tr_cells[l][i][j]))
            riza=math.sqrt(diff)
            summ+=riza
        weights[k][l]=summ
# print ('\n\n\n\n\n Weights: \n\n\n')
# print weights



################## Eucleidian shit #####################

# print (train_coords)

# print ("\n\n\n")

# print run_coords


#
# Computing the Eukleidian distance between the estimated location 
# of the AP, and the actual location
#
# Note: math.pow function's field, seen with the value "2"
# is for the 2nd power of a number. 
#

distance = []
for i in range(0,35):
    indx = weights[i].index(min(weights[i]))
    xdif = math.pow((run_coords[i][0]*0.55) - (train_coords[indx][0]*0.55) ,2)
    ydif = math.pow((run_coords[i][1]*0.55) - (train_coords[indx][1]*0.55) ,2)
    distance.append(math.sqrt(xdif+ydif))

print distance



################## cdf #####################
num_bins = 200
counts, bin_edges = np.histogram (distance, bins=num_bins, normed=True)
cdf = np.cumsum (counts)
plt.plot (bin_edges[1:], cdf/cdf[-1])
plt.show()

################## median error #####################

median_err = np.median(distance)

print ("Median error = {}".format(median_err))

